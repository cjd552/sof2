import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

public class SocialNetwork {
    String name; // name of the Network

    /*
     * A map represnting the network of users.
     * The key is the user ID and the values are User objects.
     */
    Map<String, User> users;

    /**
     * A class that represents connections between users.
     * @param networkname the name of the network
     */
    public SocialNetwork(String networkname) {
        name = networkname;
        users = new HashMap<String, User>(); // HashMap is used so users can be found by ID
    }

    /**
     * Creates a new User object and adds it to the network. Also returns this object.
     * @param userID The ID for the new User object. This must be unique.
     * @param userName The name for the new User object which does not have to be unique.
     * @return newUser A new User object with ID userID and name userName.
     * @throws IllegalArgumentException when the userID already is the ID of an existing user in the network
     */
    public User createUser(String userID, String userName) throws IllegalArgumentException {
        /* if the user ID is already in use throw an exception */
        if (users.get(userID) != null) 
        {
            throw new IllegalArgumentException("This ID is already taken");
        };
        /* otherwise create the new User object and return it */
        User newUser = new User(userID, userName);
        users.put(userID, newUser);
        return newUser;
    }

    /**
     * Locates a user based on their ID and returns their User object.
     * @param userID the ID of the user to be found
     * @return toreturn The User object in the users network whose ID (key) is userID
     * @throws IllegalArgumentException when a user with ID userID cannot be found
     */
    public User getUser(String userID) throws IllegalArgumentException {
        User toreturn = users.get(userID);
        /* if the user ID isn't in use throw an exception */
        if (toreturn == null) 
        {
            throw new IllegalArgumentException("No such user!");
        }
        return toreturn;
    }

    /**
     * Forms a new connection between two users by updating both of their connection attributes.
     * If the two users were already connected, the connection fails, otherwise it is successful.
     * @param userOneID the ID of the first user involved in the connection.
     * @param userTwoID the ID of the second user involved in the connection.
     * @return true if the connections both ways were successful and false if one or both fails.
     */
    public boolean addRelationship(String userOneID, String userTwoID) {
        /* getUser will raise IllegalArgumentException if user is not found */
        User userOne = getUser(userOneID); 
        User userTwo = getUser(userTwoID);
        /* if either of the connections failed, false is returned */
        return userOne.addConnection(userTwoID) && userTwo.addConnection(userOneID);
    }

    /**
     * Calculates the normalised closeness centrality of a user vertex in the graph represented by the 'users' map.
     * The vertex to find the closeness of is signified by the provided userID.
     * This is done using Bavelas' formula: (N-1) / (sum of shortest distance to all other vertices)
     * This assumes that every node is reachable from every other node.
     * @param userID the ID of the user for which the corresponding vertex's closeness should be calculated
     * @return the closeness number for the given user vertex.
     */
    public double closeness(String userID) {
        User thisUser = getUser(userID); // will raise IllegalArgumentException if user is not found
        Integer N = users.size(); // number of vertices in the user graph
        /* Apply Dijkstra's algorithm */
        double distanceSum = shortestDistance(thisUser);
        /* Apply Bavelas' formula */
        return (N-1) / distanceSum;
    }

    /**
     * Uses Dijkstra's Algorithm to find the shortest distance to every other vertex in the graph represented
     * by the 'users' map. Two connected users have a distance of 1 from each other.
     * @param thisUser the user object representing the starting vertex.
     * @return distanceSum The sum of the shortest distances to each vertex.
     */
    private Integer shortestDistance(User thisUser) {
        Map<String,Integer> distances = new HashMap<String, Integer>(); // map of user ID and distance

        /* initialise unvisited to a copy of the set of all user IDs since no vertices are visited at the start*/
        Set<String> unvisited = new HashSet<String>(users.keySet()); 

        Integer distanceSum = 0;

        
        /* initialise distance to a very large value for all vertices as all distances are currently unknown */
        for (String key: users.keySet()) {
            distances.put(key, 11111); 
        } 

        distances.put(thisUser.id, 0); // set the distance to the starting vertex to 0
        
        while (unvisited.size() > 0) {
            String minKey = null; // to store the key for the closest user
            Integer minDist = 11111; // to store the distance to the current closest user

            /** iterate through all unvisited vertices to find the closest one */
            for (String currentKey: unvisited) {
                Integer currentDist = distances.get(currentKey);
                if (currentDist < minDist) {
                    minKey = currentKey;
                    minDist = currentDist;
                } // end if
            } // end for

            /** visit the closest vertex, so remove it from unvisited and add its distance to the total */
            unvisited.remove(minKey);
            distanceSum += distances.get(minKey);

            /** check whether visiting other vertices from the current vertex would reduce any distances
             * we only need to check this for unvisited vertices, so
             * we use the set of vertices in the intersection between the set 'unvisited' and the set of users connected to the last visited vertex
            */
            Set<String> unvisitedConnections = new HashSet<String>(unvisited);
            unvisitedConnections.retainAll(users.get(minKey).getConnections());

            /* iterate through all the unvisited connections and compare their existing shortest distance to the new potential one */
            for (String currentID : unvisitedConnections) {
                Integer alt = minDist + 1; // since the distance between any two vertices is always 1 due to the graph being unweighted
                if (alt < distances.get(currentID)){
                    distances.put(currentID, alt);
                } // end if
            } // end for
        } // end while
        return distanceSum;
    }
}
