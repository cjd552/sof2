import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class LevelChecker {

    /**
     * Uses a brute force algorithm to check the feasability of a level.
     * A level is feasable if there's a possible path from the first to the last board.
     * @param level The level to check the feasability of.
     * @return true if the level is feasable and false if it isn't.
     */
    public static boolean check(int[] level) {
        if (level.length == 1 && (level[0] > 0)) {
            /* landed at the exit */
            return true;
        }

        if (level[0] == 0) {
            /* landed on a mine */
            return false;
        }

        /* The maximum distance in number of boards that the player can go from the current board.*/
        Integer maxJump = Integer.min(level[0], level.length - 1);

        for (int i = 1; i <= maxJump; i++) {
            int[] A = Arrays.copyOfRange(level, i, level.length);
            if (check(A)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Used to generate an adjacency list for a graph represntation of the game level.
     * There is a connection from A to B if it's possible to jump
     * from A to B, and the weight being the number of jumps that would take.
     * @param level An array of positive integers representing a game level.
     * @return connections A list where each element is the set of podiums that can be reached from the podium in the position of that index in the list.
     */
    private static List<Set<Integer>> getConnections(int[] level){
        List<Set<Integer>> connections = new ArrayList<Set<Integer>>();
        for (int i = 0; i < level.length; i++){
            Set<Integer> connectionSet = new HashSet<Integer>();
            for (int j = 1; j <= level[i] && i+j < level.length; j++){
                if (level[i+j] != 0) {
                    connectionSet.add(i+j);
                }
            } // end inner for
            connections.add(i, connectionSet);
        } // end outer for
        return connections;
    }

    /**
     * Returns the shortest list of jumps to pass the level if the level is feasable, an empty list otherwise.
     * Repurposed Dijstra's algorithm - we imagine the podiums as vertices in a graph, with a connection from A to B if it's possible to jump
     * from A to B, and the weight being the number of jumps that would take. 
     * However, keeping track of the distances is actually pointless, because the total number of jumps to get to the second podium will always be 1,
     * the total number of jumps to get to the third one is 2 and so on. 
     * @param level An array of positive integers representing a game level.
     * @return The list of jumps to be made to get from the first podium to the last one
     */
    public static List<Integer> getJumps(int[] level) {
        Map<Integer,Boolean> visited = new HashMap<Integer, Boolean>(); // map of position in level and whether it has been reached

        /* Obtain a list of all the reachable podiums from each podium - their "connections" if they were in a graph. */
        List<Set<Integer>> connections = getConnections(level);

        /* Lists for each podium, of the number of jumps at each podium visited required to get to that point*/
        Map<Integer,List<Integer>> jumpsToHere = new HashMap<Integer,List<Integer>>();

        /* initialise unprocessed to a copy of the set of all positions since no vertices are visited at the start
         * initialise all visited values to false as no podiums have been visited
         * and initialise jumpsToHere with empty lists    
        */
        Set<Integer> unprocessed = new HashSet<Integer>();
        for (int i = 0; i < level.length; i++) {
            unprocessed.add(i);
            List<Integer> emptyList = new ArrayList<Integer>();
            visited.put(i, false); 
            jumpsToHere.put(i, emptyList);
        }

        visited.put(0, true); // visit the starting vertex
        
        /* objective is to get the list of jumps to get to the end, so loop until the last vertex is processed */
        while (unprocessed.contains(level.length-1) && visited.containsValue(true)) {
            Integer currentProcessingPodium = null; // to store the key for the podium we currently are processing

            /** only podiums that have been reached can be processed*/
            for (Integer currentKey: unprocessed) {
                if (visited.get(currentKey)){
                    currentProcessingPodium = currentKey;
                    unprocessed.remove(currentProcessingPodium);
                    break;
                };
            } // end for

            visited.remove(currentProcessingPodium);

            /* see if we can visit any unprocessed podiums which are in the set of podiums in the intersection
             * between the set 'unprocessed' and the set of users connected to the last visited vertex
            */
            Set<Integer> unprocessedConnections = new HashSet<Integer>(unprocessed);
            unprocessedConnections.retainAll(connections.get(currentProcessingPodium));

            /* visit all reachable unprocessed podiums and generate their jump lists */
            for (Integer currentID : unprocessedConnections) {
                if (!visited.get(currentID)){
                    visited.put(currentID, true);
                    List<Integer> newJumps = new ArrayList<Integer>(jumpsToHere.get(currentProcessingPodium));
                    newJumps.add(currentID - currentProcessingPodium);
                    jumpsToHere.put(currentID, newJumps);
                } // end if
            } // end for
        } // end while

        return jumpsToHere.get(level.length-1);
    }

    /**
     * Uses a more dynamic approach to check the feasability of a level.
     * A level is feasable if there's a possible path from the first to the last board.
     * We imagine the podiums as vertices in a graph, with a connection from A to B if it's possible to jump
     * from A to B, and the weight being the number of jumps that would take. 
     * Based on Depth First Search, but instead of recursion, a stack is used to process each vertex iteratively,
     * and a set is used to remember which ones have already been processed so repetition is avoided.
     * @param level The level to check the feasability of.
     * @return true if the level is feasable and false if it isn't.
     */
    public static boolean betterCheck(int [] level){
        List<Set<Integer>> connections = getConnections(level);
        Stack<Integer> DFSstack = new Stack<Integer>();
        Set<Integer> reachable = new HashSet<Integer>();

        DFSstack.push(0); // we start at the first podium

        while (!DFSstack.empty()) {
            Integer currentID = DFSstack.pop();
            /* Reachable contains all the podiums that we've already processed so its connections will already have been on the stack.
             * Because this information was remembered, we save on processing time.
             * */
            if (!reachable.contains(currentID)){
                reachable.add(currentID);
                for (Integer connectedID : connections.get(currentID)) {
                    DFSstack.push(connectedID);
                    /* We stop processing as soon as the end vertex is reached, as whether or not the other podiums can be reached is irrelevant.*/
                    if (connectedID == level.length-1) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}